# hello-react-node-postgres

An Adapt starter project that includes:
* A React front end, created with create-react-app
* A Node.js HTTP server back end that responds with Hello World
* A static HTTP server to serve HTML, CSS, and other static content
* A Postgres database

